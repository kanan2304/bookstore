package com.example.bookStore.model;

public enum BookStatus {
    Vacant,
    Incomplete,
    Closed

}
