package com.example.bookStore.service;

import com.example.bookStore.config.security.BaseJwtService;
import com.example.bookStore.dto.*;
import com.example.bookStore.exception.BadRequestException;
import com.example.bookStore.exception.ErrorCodes;
import com.example.bookStore.model.Authority;
import com.example.bookStore.model.Users;
import com.example.bookStore.repository.AuthorityRepository;
import com.example.bookStore.repository.BookRepository;
import com.example.bookStore.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService{
    private final UserRepository userRepository;
    private final BookRepository taskRepository;
    private final AuthorityRepository authorityRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final BaseJwtService baseJwtService;

    public UserResponse getUserFromDb(GetUserFromDb getUserFromDb) {
        Users usersFromDb = userRepository.getUsersFromDb(getUserFromDb.getEmail());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if(encoder.matches(getUserFromDb.getPassword(), usersFromDb.getDefaultPassword())){
            return modelMapper.map(usersFromDb, UserResponse.class);
        }
        throw new BadRequestException(ErrorCodes.PASSWORD_MISMATCH);
    }
    @Cacheable(cacheNames = "users", key = "#root.methodName")
    public List<Users> findAll() {
        return userRepository.findAll();
    }
    public UserResponse createUser(UserRequest userRequest) {
        Optional<Users> user1 = Optional.ofNullable(userRepository.getUsersFromDb(userRequest.getEmail()));
        if (user1.isPresent()){
            throw new BadRequestException(ErrorCodes.USER_ALREADY_PRESENT);
        }
        Users user = modelMapper.map(userRequest, Users.class);
        Authority authority = authorityRepository.findById(1L).orElseThrow(() -> new RuntimeException("authority not found"));
        user.setAuthorities(List.of(authority));
        user.setDefaultPassword(bCryptPasswordEncoder.encode(user.getDefaultPassword()));
        userRepository.save(user);
        UserResponse map = modelMapper.map(user, UserResponse.class);


//        String[] chunks = token.split("\\.");
//
//        Base64.Decoder decoder = Base64.getUrlDecoder();
//
//        String header = new String(decoder.decode(chunks[0]));
//        String payload = new String(decoder.decode(chunks[1]));


        return UserResponse.builder()
                .id(map.getId())
                .name(map.getName())
                .email(map.getEmail())
                .surname(map.getSurname())
                .jwt(baseJwtService.issueToken(user))
                .build();
    }
    public UserResponse updateUser(Long userId, UserRequest userRequest) {
        Users users = userRepository.findById(userId).orElseThrow(() ->new BadRequestException(ErrorCodes.USER_IS_NOT_PRESENT));

        modelMapper.map(userRequest, Users.class);

        users.setName(userRequest.getName());
        users.setSurname(userRequest.getSurname());
        users.setEmail(userRequest.getEmail());
        users.setDefaultPassword(bCryptPasswordEncoder.encode(userRequest.getDefaultPassword()));

        Users save = userRepository.save(users);

        return modelMapper.map(save, UserResponse.class);
    }
    public void deleteUser(Long userId) {
        userRepository.findById(userId).orElseThrow(() -> new BadRequestException(ErrorCodes.USER_IS_NOT_PRESENT));
        userRepository.deleteById(userId);
    }
}
