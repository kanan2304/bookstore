package com.example.bookStore.service;

import com.example.bookStore.dto.BookAssignToUsers;
import com.example.bookStore.dto.BookRequest;
import com.example.bookStore.dto.BookResponse;
import com.example.bookStore.exception.BadRequestException;
import com.example.bookStore.exception.ErrorCodes;
import com.example.bookStore.model.Administrator;
import com.example.bookStore.model.Book;
import com.example.bookStore.model.Users;
import com.example.bookStore.repository.AdministratorRepository;
import com.example.bookStore.repository.BookRepository;
import com.example.bookStore.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class BookService {
    private final BookRepository taskRepository;
    private final UserRepository userRepository;
    private final AdministratorRepository administratorRepository;
    private final ModelMapper modelMapper;
    private final KafkaTemplate<String, Object> kafkaTemplate;



    public String convert(Set<String> set) {
        return String.join(" ", set);
    }
    @Cacheable(cacheNames = "task", key = "#root.methodName")
    public List<Book> findAll() {
        return taskRepository.findAll();
    }
    public ResponseEntity<String> assignUsersToBook(BookAssignToUsers taskAssignToUsers) {
        Book book = taskRepository.findByName(taskAssignToUsers.getTitle()).orElseThrow(() -> new BadRequestException(ErrorCodes.TASK_IS_NOT_PRESENT));
        String convert = convert(taskAssignToUsers.getEmail());
        Optional<Users> usersEmailFromDb = Optional.ofNullable(userRepository.getUsersEmailFromDb(convert).orElseThrow(() -> new BadRequestException(ErrorCodes.USER_IS_NOT_PRESENT)));
        usersEmailFromDb.get().setBook(book);
        kafkaTemplate.send("task-assign-topic", taskAssignToUsers);
        return ResponseEntity.ok("Task Assignment is completed");
    }
    public BookResponse getBook(Long bookId) {
//        userRepository.findById(userId).orElseThrow(() -> new RuntimeException(String.format("User with id %s not found", userId)));
        Book book = taskRepository.findById(bookId).orElseThrow(() -> new BadRequestException(ErrorCodes.TASK_IS_NOT_PRESENT));
        return modelMapper.map(book, BookResponse.class);
    }
    public BookResponse createBook(BookRequest taskRequest) {
        Administrator adminFromDb = administratorRepository.getAdminFromDb(taskRequest.getAdminEmail());
        Optional<Book> byName = taskRepository.findByName(taskRequest.getTitle());
        if (byName.isPresent()){
            throw new BadRequestException(ErrorCodes.TASK_ALREADY_PRESENT);
        }
        Book task = modelMapper.map(taskRequest, Book.class);
        task.setAdministrator(adminFromDb);
        taskRepository.save(task);
        return modelMapper.map(task, BookResponse.class);
    }

    public BookResponse updateBook(Long taskId, BookRequest taskRequest) {
        Book task = taskRepository.findById(taskId).orElseThrow(() -> new BadRequestException(ErrorCodes.TASK_IS_NOT_PRESENT));

        modelMapper.map(taskRequest, Book.class);

        task.setTitle(taskRequest.getTitle());
        task.setDescription(taskRequest.getDescription());
        task.setBookStatus(taskRequest.getBookStatus());

        Book save = taskRepository.save(task);

        return modelMapper.map(save, BookResponse.class);
    }
}
