package com.example.bookStore.service;

import com.example.bookStore.config.security.BaseJwtService;
import com.example.bookStore.dto.AdministratorRequest;
import com.example.bookStore.dto.AdministratorResponse;
import com.example.bookStore.dto.GetAdminFromDbRequest;
import com.example.bookStore.exception.BadRequestException;
import com.example.bookStore.exception.ErrorCodes;
import com.example.bookStore.model.Administrator;
import com.example.bookStore.model.Authority;
import com.example.bookStore.repository.AdministratorRepository;
import com.example.bookStore.repository.AuthorityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.cache.CacheManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AdministratorService {
    private final AdministratorRepository administratorRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final AuthorityRepository authorityRepository;
    private final BaseJwtService baseJwtService;
    private final CacheManager cacheManager;

    public AdministratorResponse getAdminFromDb(GetAdminFromDbRequest getAdminFromDbRequest) {
        Administrator adminFromDb = administratorRepository.getAdminFromDb(getAdminFromDbRequest.getEmail());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if(encoder.matches(getAdminFromDbRequest.getPassword(), adminFromDb.getPassword())){
//            log.info("Start with CacheManger method");
//            Cache cache = cacheManager.getCache("admin");
//            cache.get(getAdminFromDbRequest.getEmail(), Administrator.class);
//            cache.get(getAdminFromDbRequest.getPassword(), Administrator.class);
            return modelMapper.map(adminFromDb, AdministratorResponse.class);
        }
        throw new BadRequestException(ErrorCodes.PASSWORD_MISMATCH);
    }

    public AdministratorResponse createOrganizationProfile(AdministratorRequest administratorRequest) {
        Optional<Administrator> administrator1 = Optional.ofNullable(administratorRepository.getAdminFromDb(administratorRequest.getEmail()));
        if (administrator1.isPresent()) {
            throw new BadRequestException(ErrorCodes.USER_ALREADY_PRESENT);
        }
        Administrator administrator = modelMapper.map(administratorRequest, Administrator.class);
        Authority authority = authorityRepository.findById(2L).orElseThrow(() -> new RuntimeException("authority not found"));
        administrator.setAuthorities(List.of(authority));
        administrator.setPassword(bCryptPasswordEncoder.encode(administrator.getPassword()));
        administrator.setPhoneNumber(administratorRequest.getPhoneNumber());
        administratorRepository.save(administrator);
        AdministratorResponse map = modelMapper.map(administrator, AdministratorResponse.class);

        return AdministratorResponse.builder()
                .id(map.getId())
                .username(map.getUsername())
                .age(map.getAge())
                .email(map.getEmail())
                .jwt(baseJwtService.issueTokenA(administrator))
                .build();
    }

    public AdministratorResponse updateOrganizationProfile(Long administratorId, AdministratorRequest administratorRequest) {
        Administrator administrator = administratorRepository.findById(administratorId).orElseThrow(() -> new BadRequestException(ErrorCodes.ADMIN_IS_NOT_PRESENT));

        modelMapper.map(administratorRequest, Administrator.class);

        administrator.setUsername(administratorRequest.getUsername());
        administrator.setEmail(administratorRequest.getEmail());
        administrator.setPassword(bCryptPasswordEncoder.encode(administratorRequest.getPassword()));
        administrator.setAddress(administratorRequest.getAddress());
        administrator.setPhoneNumber(administratorRequest.getPhoneNumber());

        Administrator save = administratorRepository.save(administrator);

        return modelMapper.map(save, AdministratorResponse.class);
    }

    public void deleteAdministrator(Long id) {
        administratorRepository.deleteById(id);
    }
}
