package com.example.bookStore.config;

import com.example.bookStore.config.security.AuthFilterConfigurerAdapter;
import com.example.bookStore.config.security.TokenAuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {
    private final TokenAuthService tokenAuthService;

    @Bean
    public SecurityFilterChain config(HttpSecurity http) throws Exception{
//        http.authorizeHttpRequests(auth-> auth.requestMatchers(GET, "/v1").permitAll());
        // /v1/hello - public
        // /v1/hello/1 - authenticated
//        http.authorizeHttpRequests(auth-> auth.requestMatchers("/v1/**").permitAll());
        // /v1/hello - public
        // /v1/hello/1 - public


        http.csrf(csrf -> csrf.disable());

        http.authorizeHttpRequests(auth-> auth.requestMatchers(POST , "/adminOrg/create").permitAll());
        http.authorizeHttpRequests(auth-> auth.requestMatchers(GET , "/adminOrg/getAdmin").permitAll());
        http.authorizeHttpRequests(auth-> auth.requestMatchers(POST , "/adminBook/publish/{message}").permitAll());

        http.authorizeHttpRequests(auth -> auth.requestMatchers("/adminOrg/*").hasAuthority("ADMIN"));
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/adminBook/getBook/{bookId}").hasAnyAuthority("ADMIN","USER"));
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/adminBook/listAllBooks").hasAnyAuthority("ADMIN", "USER"));
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/adminBook/deleteBook/{bookId}").hasAuthority("ADMIN"));
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/adminBook/***").hasAuthority("ADMIN"));
//        http.authorizeHttpRequests(auth -> auth.requestMatchers("/adminUser/createUser").hasAuthority( "ADMIN"));
//        http.authorizeHttpRequests(auth -> auth.requestMatchers("/adminUser/**").hasAnyAuthority("ADMIN", "USER"));

//        http.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());
        http.authorizeHttpRequests(auth -> auth.anyRequest().permitAll());
        http.apply(new AuthFilterConfigurerAdapter(tokenAuthService));

        return http.build();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

}
