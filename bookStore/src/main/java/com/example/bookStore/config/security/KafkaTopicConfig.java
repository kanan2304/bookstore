package com.example.bookStore.config.security;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {
    @Bean
    public NewTopic taskAssignTopic(){
        return TopicBuilder.name("task-assign-topic")
                .partitions(3)
                .replicas(1)
                .build();
    }
}
