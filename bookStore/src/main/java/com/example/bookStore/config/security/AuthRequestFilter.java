package com.example.bookStore.config.security;


import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
public class AuthRequestFilter extends OncePerRequestFilter {

//    private final List<AuthService> authServiceList;
    private final TokenAuthService tokenAuthService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws IOException, ServletException {
//        log.trace("Filtering request against auth services {}", authServices);
//        Optional<Authentication> authOptional = Optional.empty();
//        for (AuthService authService : authServices) {
//            authOptional = authOptional.or(() -> authService.getAuthentication(httpServletRequest));
//        }
//        authOptional.ifPresent(auth -> SecurityContextHolder.getContext().setAuthentication(auth));
        Optional<Authentication> authentication = tokenAuthService.getAuthentication(httpServletRequest);
        authentication.ifPresent(auth ->{
                SecurityContextHolder.getContext().setAuthentication(auth);
                log.info("AUTHENTICATION IS {}", auth);
    });
        log.info("Auth Request Filter is working !!!");
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
