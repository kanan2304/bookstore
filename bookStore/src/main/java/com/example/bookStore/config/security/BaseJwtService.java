package com.example.bookStore.config.security;

import com.example.bookStore.model.Administrator;
import com.example.bookStore.model.Authority;
import com.example.bookStore.model.Users;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public final class BaseJwtService {

    @Value("${spring.security.secret}")
    private String secret;
    @Value("${security.token.expirationTime}")
    private String expTimeInMinutes;
    private Key key;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(secret);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueTokenA(Administrator administrator){
        return Jwts.builder()
                .setSubject(administrator.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(Long.parseLong(expTimeInMinutes)))))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS512)
                .claim("authority", administrator.getAuthorities().stream().map(Authority::getAuthority).collect(Collectors.toList()))
                .compact();
    }

    public String issueToken(Users users){
        return Jwts.builder()
                .setSubject(users.getName())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(Long.parseLong(expTimeInMinutes)))))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS512)
                .claim("authority", users.getAuthorities().stream().map(Authority::getAuthority).collect(Collectors.toList()))
                .compact();
    }
}
