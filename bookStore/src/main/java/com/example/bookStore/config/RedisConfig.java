package com.example.bookStore.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.cache.RedisCacheManagerBuilderCustomizer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
@ConfigurationProperties("redis")
@EnableConfigurationProperties(RedisConfig.class)
@Data
@EnableCaching
public class RedisConfig {

//    private static final long EXPIRE_TIME=600;
//    private static final String CODE_SIGN_ENROLLMENT_TEMP_CACHE_NAME = "cs_-enrollment-temp-cache";

    private String host;
    private Integer port;
    private String password;
    private String username;
    private String prefix;

    @Bean
    public RedisCacheManagerBuilderCustomizer cacheManagerCustomizer() {
        return builder -> {
//            Map<String, RedisConfiguration> configurationMap = new HashMap<>();
//            configurationMap.put("phone",
//                    RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(15)));
//            configurationMap.put("account",
//                    RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofSeconds(10)));
//            builder.withInitialCacheConfigurations(configurationMap);
        };
    }
    @Bean
    public JedisConnectionFactory jedisConnectionFactory(){
        var configuration = new RedisStandaloneConfiguration();
        configuration.setHostName(host);
        configuration.setPort(port);
        configuration.setPassword(password);
        configuration.setUsername(username);
        return new JedisConnectionFactory(configuration);
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(){
        final RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        var stringRedisSerializer = new StringRedisSerializer();
        template.setKeySerializer(stringRedisSerializer);
        template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        template.setHashKeySerializer(stringRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }

}
