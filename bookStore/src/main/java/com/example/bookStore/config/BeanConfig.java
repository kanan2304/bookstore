package com.example.bookStore.config;

import com.example.bookStore.model.Users;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {
    @Bean
    public ModelMapper getInstance(){
        return new ModelMapper();
    }
    @Bean
    public Users users(){
        return new Users();
    }
}
