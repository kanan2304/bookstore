package com.example.bookStore.repository;

import com.example.bookStore.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    @Query(value = "select * from Authority where authority = ?1 ", nativeQuery = true)
    List<Authority> findByUsername(String username);
}
