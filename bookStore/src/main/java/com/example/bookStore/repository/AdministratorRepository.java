package com.example.bookStore.repository;

import com.example.bookStore.model.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AdministratorRepository extends JpaRepository<Administrator, Long> {
    Administrator findByUsername(String username);


    @Query(value = "select a from Administrator a where a.email = :email")
    Administrator getAdminFromDb(String email);
}
