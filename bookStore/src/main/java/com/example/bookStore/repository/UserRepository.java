package com.example.bookStore.repository;

import com.example.bookStore.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, Long > {
//    @Cacheable(cacheNames = "users", key = "#email")
    @Query(value = "select u from Users u where u.name = :name")
    Optional<Users> getUserFromDb(String name);

    @Query(value = "select u from Users u where u.email = :email")
    Users getUsersFromDb(String email);
    @Query(value = "select u from Users u where u.email = :email")
    Optional<Users> getUsersEmailFromDb(String email);

//    @EntityGraph(attributePaths = "task")
//    List<Users> findAll();
}
