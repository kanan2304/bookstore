package com.example.bookStore.controller;

import com.example.bookStore.dto.*;
import com.example.bookStore.model.Book;
import com.example.bookStore.repository.BookRepository;
import com.example.bookStore.service.BookService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/adminBook")
@RequiredArgsConstructor
public class BookController {
    private final BookService taskService;
    private final BookRepository bookRepository;
    private final KafkaTemplate<String , String> kafkaTemplate;
    private final KafkaTemplate<String, Object> objectKafkaTemplate;

    @PostMapping("/publish/{message}")
    public ResponseEntity<Void> publish(@RequestBody String message){
        kafkaTemplate.send("task-assign-topic", "1", message);
        return ResponseEntity.ok().build();
    }
    @PostMapping("/createBook")
    public BookResponse createTask(@RequestBody @Valid BookRequest taskRequest){
        return taskService.createBook(taskRequest);
    }
    @PostMapping("/assignUsersToBook")
    public ResponseEntity<String> assignUsersToTask(@RequestBody BookAssignToUsers taskAssignToUsers){
        return taskService.assignUsersToBook(taskAssignToUsers);
    }
    @PutMapping("updateBook/{bookId}")
    public BookResponse updateTask(@PathVariable Long taskId, @RequestBody BookRequest taskRequest){
        return taskService.updateBook(taskId,taskRequest);
    }
    @GetMapping("/listAllBooks")
    public List<Book> findAll() {
        return taskService.findAll();
    }
    @GetMapping("/getBook/{bookId}")
    public BookResponse getTask(@PathVariable Long bookId){
        return taskService.getBook(bookId);
    }
    @DeleteMapping("/deleteBook/{bookId}")
    public void deleteTask(@PathVariable Long bookId){
        bookRepository.deleteById(bookId);
    }
}
