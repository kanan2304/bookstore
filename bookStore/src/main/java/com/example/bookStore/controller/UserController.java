package com.example.bookStore.controller;

import com.example.bookStore.dto.*;
import com.example.bookStore.model.Users;
import com.example.bookStore.repository.UserRepository;
import com.example.bookStore.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/adminUser")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final UserRepository userRepository;

    @PostMapping("/createUser")
    public UserResponse createUser(@RequestBody UserRequest userRequest){
        return userService.createUser(userRequest);
    }
    @PutMapping("/{userId}")
    public UserResponse updateUser(@PathVariable Long userId, @RequestBody UserRequest userRequest){
        return userService.updateUser(userId, userRequest);
    }
    @GetMapping("/listAllUsers")
    public List<Users> findAll() {
        return userService.findAll();
    }
    @GetMapping("/getUser")
    @Cacheable(cacheNames = "getUser", key = "#getUserFromDb.email+#getUserFromDb.password" )
    public UserResponse getUserFromDb(@RequestBody GetUserFromDb getUserFromDb){
        return userService.getUserFromDb(getUserFromDb);
    }
    @DeleteMapping("/deleteUser/{userId}")
    public void deleteUser(@PathVariable Long userId){
        userService.deleteUser(userId);
    }
}
