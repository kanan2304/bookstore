package com.example.bookStore.controller;

import com.example.bookStore.dto.AdministratorRequest;
import com.example.bookStore.dto.AdministratorResponse;
import com.example.bookStore.dto.GetAdminFromDbRequest;
import com.example.bookStore.repository.AuthorityRepository;
import com.example.bookStore.service.AdministratorService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/adminOrg")
@RequiredArgsConstructor
public class AdministratorController {
    private final AdministratorService administratorService;
    private final AuthorityRepository administratorRepository;
    @PostMapping("/create")
    public AdministratorResponse createOrganizationProfile(@RequestBody @Valid AdministratorRequest administratorRequest){
        return administratorService.createOrganizationProfile(administratorRequest);
    }
    @PutMapping("/{administratorId}")
    public AdministratorResponse updateOrganizationProfile(@PathVariable Long administratorId, @RequestBody AdministratorRequest administratorRequest){
        return administratorService.updateOrganizationProfile(administratorId,administratorRequest);
    }
    @GetMapping("/getAdmin")
    @Cacheable(cacheNames = "getAdmin", key = "#getAdminFromDbRequest.email")
    public AdministratorResponse getAdminFromDb(@RequestBody GetAdminFromDbRequest getAdminFromDbRequest){
        return administratorService.getAdminFromDb(getAdminFromDbRequest);
    }

    @DeleteMapping("/{administratorId}")
    public void deleteOrganizationProfile(@PathVariable Long administratorId){
        administratorService.deleteAdministrator(administratorId);
    }
}
