package com.example.bookStore;

import com.example.bookStore.model.Authority;
import com.example.bookStore.repository.AuthorityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.security.authorization.AuthorizationObservationContext;

@SpringBootApplication
@RequiredArgsConstructor
@EnableKafka
public class BookStoreApplication implements CommandLineRunner {
    private final AuthorityRepository authorityRepository;

    public static void main(String[] args) {
        SpringApplication.run(BookStoreApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
//        Authority user = Authority.builder()
//                .authority("USER")
//                .build();
//
//        Authority admin = Authority.builder()
//                .authority("ADMIN")
//                .build();
//
//        authorityRepository.save(user);
//        authorityRepository.save(admin);
    }
}
