package com.example.bookStore.dto;

import com.example.bookStore.model.Administrator;
import com.example.bookStore.model.BookStatus;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookResponse {
    Long id;
    String title;
    String description;

    BookStatus bookStatus;

    Administrator administrator;

    String jwt;
}
