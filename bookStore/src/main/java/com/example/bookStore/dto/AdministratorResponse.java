package com.example.bookStore.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdministratorResponse implements Serializable {
    Long id;
    String username;
    Integer age;
    String email;
    String jwt;
}
