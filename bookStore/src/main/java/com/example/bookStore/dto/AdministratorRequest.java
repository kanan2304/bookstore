package com.example.bookStore.dto;

import jakarta.validation.constraints.Pattern;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdministratorRequest {
    String username;
    String email;
    Integer age;
    @Pattern(regexp = "^[a-zA-Z0-9]{6,}$")
    String password;
    String phoneNumber;
    String address;
}
