package com.example.bookStore.dto;

import jakarta.validation.constraints.Pattern;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequest {
    String name;
    String surname;
    String email;
    @Pattern(regexp = "^[a-zA-Z0-9]{6,}$")
    String defaultPassword;
}
