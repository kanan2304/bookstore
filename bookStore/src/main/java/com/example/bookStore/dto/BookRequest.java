package com.example.bookStore.dto;

import com.example.bookStore.model.BookStatus;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookRequest {
    String title;
    String description;

    BookStatus bookStatus;

    String adminEmail;
}
