package com.example.bookStore.exception;

public enum ErrorCodes {
        USER_ALREADY_PRESENT,
    PASSWORD_MISMATCH,
    TASK_ALREADY_PRESENT,
    TASK_IS_NOT_PRESENT,
    ADMIN_IS_NOT_PRESENT,
    USER_IS_NOT_PRESENT
}
