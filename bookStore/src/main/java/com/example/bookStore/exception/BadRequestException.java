package com.example.bookStore.exception;

import lombok.Getter;

@Getter
public class BadRequestException extends RuntimeException{
    public final ErrorCodes errorCode;
    public final transient Object[] arguments;
    public BadRequestException(ErrorCodes errorCode, Object ...  arguments) {
        this.errorCode = errorCode;
        this.arguments = arguments == null ? new Object[0] : arguments;
    }
}
