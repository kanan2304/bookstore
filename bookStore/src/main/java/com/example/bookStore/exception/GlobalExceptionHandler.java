package com.example.bookStore.exception;

import com.example.bookStore.dto.ErrorResponseDto;
import com.example.bookStore.service.TranslationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {
    private final TranslationService translationService;

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorResponseDto> handleBadRequestException(BadRequestException ex, WebRequest req) {
        var lang = req.getHeaderValues(ACCEPT_LANGUAGE) ==  null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();
//        String path = ((ServletWebRequest) req).getRequest().getRequestURL().toString();
//        String requestedLanguage = AcceptLanguageInterceptor.getLocale().getLanguage();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang, ex.getArguments()))
                .build());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, WebRequest req){
        ex.printStackTrace();
        ErrorResponseDto response = ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .build();
        ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .forEach(error -> {
                    Map<String, String> data = response.getData();
                    data.put(error.getField(), error.getDefaultMessage());
                });
        return ResponseEntity.status(400).body(response);
    }
}
